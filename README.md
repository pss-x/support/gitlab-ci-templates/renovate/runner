# Renovate Runner

[Renovate Bot](https://github.com/renovatebot/renovate) keeps your dependencies up-to-date.

The aim of this project is to provide a Renovate configuration created for pssx modules or application projects.

### Functional user and credentials

You need to add a GitLab [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token) (scopes: `read_user`, `api` and `write_repository`) as `RENOVATE_TOKEN` to CI/CD variables.
You can also use a GitLab [Group Access Token](https://docs.gitlab.com/ee/user/group/settings/group_access_tokens.html).
Checkout Renovate platform [docs](https://docs.renovatebot.com/modules/platform/gitlab/) and #53 for more information.

It is also recommended to configure a [GitHub.com Personal Access Token](https://docs.github.com/en/free-pro-team@latest/github/authenticating-to-github/creating-a-personal-access-token) (minimum scopes) as `GITHUB_TOKEN` so that your bot can make authenticated requests to github.com for Changelog retrieval as well as for any dependency that uses GitHub tags.
Without such a token, github.com's API will rate limit requests and make such lookups unreliable.

A functional bot user can be configured to trigger the scheduled builds on gitlab.com.
All Renovate activity (Merge Requests, Issues and commits) is performed with the identity of this user. 
You can configure a different commit author via the [`gitAuthor`](https://docs.renovatebot.com/configuration-options/#gitauthor) option.

You will also have to give the bot user access to the relevant projects, as it needs push rights in order to create Merge Requests. Make also sure the path of the branches renovate creates (by default under prefix `renovate/*`) is not protected, or renovate won't be able to force-push on them, effectively being unable to keep the MRs up-to-date.

You will then need to configure at least the following CI variables:

* `RENOVATE_TOKEN` a [PAT](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html) belonging to your bot account
* (optional) `GITHUB_TOKEN` for github.com changelog retrieval
* `RENOVATE_EXTRA_FLAGS` a space-separated list choosing which projects to renovate

### Self-hosting Renovate

Hosting Renovate

- PSSX provides some infrastructure for Renovate to run on (by running a Docker image)
- You need to provide a global configuration file called [config.js](https://docs.renovatebot.com/getting-started/running/#global-config).
- You need to add a per-repository configuration file called [renovate.json](https://docs.renovatebot.com/configuration-options/).
- Renovate needs to run on a schedule 

### Configure the Schedule

Add a schedule (`Build` > `Pipeline Schedules`) to run Renovate regularly.

A good practise is to run it hourly. The following runs Renovate on the third minute every hour: `0 * * * *`.

Because the default pipeline only runs on schedules, you need to use the `play` button of schedule to trigger a manual run.

You can further customize this by supplying your own `RENOVATE_EXTRA_FLAGS` variable when triggering the manual job, following [upstream documentation](https://gitlab.com/renovate-bot/renovate-runner#configure-cicd-variables).

### GitLab CI pipeline

The minimal configuration you will need after the setup steps is to include the template contained in this repository into your own gitlab-ci file:

```yaml
include:
 - project: 'pss-x/support/gitlab-ci-templates/renovate/runner'
    ref: 'main'
    file: '/templates/.run.yml'

stages:
  - renovate:run

renovate:run:
  extends: [.renovate:run]
  stage: renovate:run
```

The renovate pipeline is called `renovate` running on schedules and can be refined by using the gitlab-ci extension model by just referencing it into your gitlab-ci file,[as the template in this repository does](./templates/.run.yml).

By default renovate won't find any repo, you need to choose one of the following options for `RENOVATE_EXTRA_FLAGS`.

If you wish for your bot to run against any project which the `RENOVATE_TOKEN` PAT has access to, but which already have a `renovate.json` or similar config file, then add this variable: `RENOVATE_EXTRA_FLAGS=`: `--autodiscover=true`.
This will mean no new projects will be onboarded.

However, we recommend you apply an `autodiscoverFilter` value like the following so that the bot does not run on any stranger's project it gets invited to: `RENOVATE_EXTRA_FLAGS`: `--autodiscover=true --autodiscover-filter=group1/*`.
Checkout renovate [docs](https://docs.renovatebot.com/gitlab-bot-security/) for more information about gitlab security.

If you wish for your bot to run against _every_ project which the `RENOVATE_TOKEN` PAT has access to, and onboard any projects which don't yet have a config, then add this variable: `RENOVATE_EXTRA_FLAGS`: `--autodiscover=true --onboarding=true --autodiscover-filter=group1/*`.

If you wish to manually specify which projects that your bot runs again, then add this variable with a space-delimited set of project names: `RENOVATE_EXTRA_FLAGS`: `group1/repo5 user3/repo1`.

## Validate Renovate configuration

renovate-bot protect also provides a template for validating Renovate configuration in downstream repositories.

```yaml
include:
 - project: 'pss-x/support/gitlab-ci-templates/renovate/runner'
    ref: 'main'
    file: '/templates/validate.yml'
    inputs:
      stage: 'test'
```

This template will add a `renovate:validate-config` job to the pipeline that ensures the Renovate configuration is valid.

### Configuration

#### RENOVATE_OPTIMIZE_FOR_DISABLED

When this option is true, Renovate will do the following during repository initialization:

Attempt to fetch the default config file (renovate.json)
Check if the file contains "enabled": false
If the file exists and the config is disabled, Renovate will skip the repo without cloning it. Otherwise, it will continue as normal.

This option is only useful where the ratio of disabled repos is quite high. It costs one extra API call per repo but has the benefit of skipping cloning of those which are disabled.

#### RENOVATE_REPOSITORY_CACHE

Set this to "enabled" to have Renovate maintain a JSON file cache per-repository to speed up extractions. Set to "reset" if you ever need to bypass the cache and have it overwritten. 
JSON files will be stored inside the cacheDir beside the existing file-based package cache.

#### RENOVATE_REQUIRE_CONFIG

Controls Renovate's behavior regarding repository config files such as renovate.json.

By default, Renovate needs a Renovate config file in each repository where it runs before it will propose any dependency updates.

You can choose any of these settings:

- "required" (default): a repository config file must be present
- "optional": if a config file exists, Renovate will use it when it runs
- "ignored": config files in the repo will be ignored, and have no effect

This feature is closely related to the onboarding config option. The combinations of requireConfig and onboarding are:

| | onboarding=true | onboarding=false | 
| ----- | ----- | ----- |
| requireConfig=required | An onboarding PR will be created if no config file exists. If the onboarding PR is closed and there's no config file, then the repository is skipped. | Repository is skipped unless a config file is added manually. |
| requireConfig=optional | An onboarding PR will be created if no config file exists. If the onboarding PR is closed and there's no config file, the repository will be processed. | Repository is processed regardless of config file presence. |
| requireConfig=ignored | No onboarding PR will be created and repo will be processed while ignoring any config file present. | Repository is processed, any config file is ignored. |

#### RENOVATE_IGNORE_PR_AUTHOR

Set to true to fetch the entire list of PRs instead of only those authored by the Renovate user

This is usually needed if someone needs to migrate bot accounts, including from the Mend Renovate App to self-hosted. If `ignorePrAuthor` is configured to true, it means Renovate will fetch the entire list of repository PRs instead of optimizing to fetch only those PRs which it created itself. You should only want to enable this if you are changing the bot account (e.g. from @old-bot to @new-bot) and want @new-bot to find and update any existing PRs created by @old-bot. It's recommended to revert this setting once that transition period is over and all old PRs are resolved.

#### RENOVATE_EXTENDS

Configuration presets to use or extend. See [shareable config presets](https://docs.renovatebot.com/config-presets/) for details. Learn how to use presets by reading the [Key concepts, Presets](https://docs.renovatebot.com/key-concepts/presets/#how-to-use-presets) page.

#### RENOVATE_LOG_FILE_LEVEL

Set the log file log level.

#### LOG_LEVEL

Log file path.

## Have a Problem or Suggestion ?

Please see a list of features and bugs under issues If you have a problem which is not listed there, please let us known.