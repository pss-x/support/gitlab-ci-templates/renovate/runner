module.exports = {
  hostRules: [
    {
      domainName: 'registry.gitlab.com',
      token: process.env.GL_TOKEN
    },
    {
      domainName: 'gitlab.com',
      token: process.env.GL_TOKEN
    },
    {
      domainName: 'github.com',
      token: process.env.GITHUB_TOKEN
    },
    {
      hostType: 'nuget',
      matchHost: 'https://gitlab.com/api/v4/groups/89584519/-/packages/nuget/index.json',
      username: process.env.GITLAB_NUGET_USERNAME,
      password: process.env.GITLAB_NUGET_PASSWORD
    },
    {
      hostType: 'npm',
      matchHost: 'https://gitlab.com/api/v4/groups/89584519/-/packages/npm/',
      token: process.env.GITLAB_NPM_PASSWORD,
      authType: 'Basic',
    },
    {
      hostType: 'npm',
      matchHost: 'https://registry.npmjs.org',
      token: process.env.NPM_TOKEN,
    },
    {
      hostType: 'docker',
      matchHost: 'https://registry.gitlab.com',
      username: process.env.GITLAB_CONTAINER_USERNAME,
      password: process.env.GITLAB_CONTAINER_PASSWORD
    }
  ],
  autodiscover: true,
  autodiscoverFilter: [
    'pss-x/applications/**',
    'pss-x/framework/**',
    'pss-x/infrastructure/**',
    'pss-x/lab/gridlab.pssx.*',
    'pss-x/modules/gridlab.pssx.*',
    'pss-x/support/gitlab-ci-templates/**',
    'pss-x/support/gitlab-ci-components/**',
    'pss-x/support/containers/**',
    'pss-x/support/terraform-modules/**',
    'pss-x/tooling/**',
    'pss-x/user-interfaces/angular/applications/**',
    'pss-x/user-interfaces/angular/modules/**',
    'pss-x/user-interfaces/blazor/applications/**',
    'pss-x/user-interfaces/blazor/modules/**'
  ], 
  automerge: false,
  baseDir: process.env.RENOVATE_BASE_DIR || '/tmp/renovate',
  dependencyDashboard: true,
  dependencyDashboardAutoclose: true,
  endpoint: process.env.RENOVATE_ENDPOINT || 'https://gitlab.com/api/v4/',
  onboarding: true,
  onboardingConfig: {
    $schema: 'https://docs.renovatebot.com/renovate-schema.json',
    extends: [
      'local>pss-x/support/gitlab-ci-templates/renovate/config:default.json5',
      'local>pss-x/support/gitlab-ci-templates/renovate/config:gitlab-ci.json5',
      'local>pss-x/support/gitlab-ci-templates/renovate/config:docker.json5',
      'local>pss-x/support/gitlab-ci-templates/renovate/config:nuget.json5',
      'local>pss-x/support/gitlab-ci-templates/renovate/config:npm.json5',
      'local>pss-x/support/gitlab-ci-templates/renovate/config:regex-docker.json5'
    ],
  },
  onboardingCommitMessage: 'chore: add configuration for renovate bot',
  optimizeForDisabled: 'true',
  platform: 'gitlab',
  prConcurrentLimit: 1000,
  prHourlyLimit: 0,
  requireConfig: 'required',
  repositoryCache: 'enabled'
};